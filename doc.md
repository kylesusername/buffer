Structures defined:

`typedef struct buffer_struct {
	size_t size;
	size_t pos;
	size_t bytes;
	FILE *stream;
	uint8_t *data;
} buffer_t`

size: the size of the buffer, in bytes
pos: the current position in the file
bytes: the number of bytes guaranteed to be read
*stream: the stream the buffer is linked to
*data: the actual buffer of data

`buffer_t *buffer_create(size_t size)`
--------------------------------------
Allocates and initializez a buffer_t

Params: size of the buffer
Returns: pointer to the buffer

`int buffer_map(buffer_t *buffer, FILE *stream)`
------------------------------------------------
Maps the buffer to a stream

Params:
	buffer struct
	stream to map to
Returns: status. 0 on success, <0 is an error.

`int buffer_map_file(buffer_t *buffer, const char *path)`
---------------------------------------------------------
Maps the buffer to a file at the given path. Opens the file.

Params:
	buffer struct
	path to file
Returns: status. 0 on success, <0 is an error.

`int buffer_advance(buffer_t *buffer, size_t advance)`
------------------------------------------------------
Will move the buffer forward in the file

Params:
	buffer struct
	number of bytes to move forward in the file
Returns: status. 0 on success, <0 is an error.

`int buffer_recede(buffer_t *buffer, size_t recede)`
----------------------------------------------------
Will move backwards in the file. Can't go further back than 0.

Params:
	buffer struct
	Number of bytes to move backwards in the file
Returns: status. 0 on success. <0 is an error.

`int buffer_close(buffer_t *buffer)`
------------------------------------
Will free the buffer, close the stream opened.

Params:
	buffer struct
Returns: status. 0 on success. <0 is an error.
