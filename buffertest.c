/* 
	Copyright (C) 2015 Kyle Dominguez <kpd@opmbx.org>

	This file is part of buffer.

    buffer is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
	by the Free Software Foundation, at version 3 of the License.

    buffer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with buffer.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

#include "buffer.h"

inline void usage(const char *argv0)
{
	printf("USAGE: %s [file]", argv0);
}

void dump_buffer(buffer_t *buffer)
{
	size_t i;
	for(i = 0; i < buffer -> bytes; ++i) {
		printf("%x", buffer -> data[i]);
	}
}

int main(int argc, char *argv[])
{
	buffer_t *buffer;
	if(argc != 2) {
		usage(argv[0]);
	}

	buffer = buffer_create(512);

	if(buffer == NULL) {
		fprintf(stderr, "problem creating the buffer\n");
		return -1;
	}

	if(buffer_map_file(buffer, argv[1]) < 0) {
		fprintf(stderr, "problem mapping file\n");
		return -2;
	}

	dump_buffer(buffer);
	
	printf("\n");

	if(buffer_advance(buffer, 512) < 0) {
		fprintf(stderr, "problem advancing buffer\n");
		return -3;
	}

	dump_buffer(buffer);

	printf("\n");

	if(buffer_recede(buffer, 512) < 0) {
		fprintf(stderr, "problem receding buffer\n");
		return -4;
	}

	dump_buffer(buffer);

	printf("\n");

	if(buffer_close(buffer) < 0) {
		fprintf(stderr, "problem closing buffer\n");
		return -5;
	}

	return 0;
}
