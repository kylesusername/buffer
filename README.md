buffer
======

The buffer header file
----------------------

To read filesystem data in buffered chunks rather than loading the entire file.
This will give programs which readfiles a small memory footprint.
